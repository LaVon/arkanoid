#include "shapes.h"
#include "board.h"

#define BALL_END  5
#define BALL_FALL 10

class Ball : public Sphere
{
public:
	Ball();
	~Ball();
	void Restart();
	void DrawBall();
	void MoveBall(float _dt);
	void FallBall();

	//����������� ������
	bool isRight, isLeft, isUp;
	void ChangeXDir();
	void ChangeYDir();
	//����� ����������� �������� (��� ��������� ������ ���������)
	GLfloat m_fStartSpeed; 
	//������� �������� (� ��������� ����������)
	GLfloat m_fSpeed;

};
