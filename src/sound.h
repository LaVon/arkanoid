//http://hashcode.ru/questions/116848/c-%D0%BF%D1%80%D0%B0%D0%B2%D0%B8%D0%BB%D1%8C%D0%BD%D0%BE%D0%B5-%D0%B8%D1%81%D0%BF%D0%BE%D0%BB%D1%8C%D0%B7%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5-map
//work with map

#include <map>
#include <string>
#include <fmod.hpp>
#include <fmod_errors.h>

using namespace std;

class Sound
{
public:
	Sound();
	~Sound();
	void PlayMySound(char* _track);
	void Update();

private:
	void InitSound();
	bool CheckError(FMOD_RESULT _result);
	void CreateStream();
	void LoadSound();

	FMOD::System* fmodSystem;

	FMOD::Sound* brickBoom;
	FMOD::Sound* boardTick;
	
	FMOD::Channel* soundChannel;

	map<string, FMOD::Sound*> trackSound;

	FMOD_RESULT result;
};

