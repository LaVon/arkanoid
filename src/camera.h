#include <iostream>
#include "shapes.h"

using namespace std;

class CamVector
{
public:
	GLfloat x, y, z;
	CamVector()
	{ 
		x = y = z = 0.0;
	};

	CamVector(float vx,float vy,float vz)
	{ x=vx; y=vy; z=vz; };

	CamVector operator += (CamVector &v)
	{
		x+=v.x;	y+=v.y;	z+=v.z; 
		return *this;
	}
	CamVector operator -= (CamVector &v)
	{
		x-=v.x;	y-=v.y;	z-=v.z;
		return *this;
	}
	CamVector operator *= (float f)
	{
		x*=f;	y*=f;	z*=f;
		return *this;
	}
};

class Camera
{
public:
	Camera();
	CamVector m_CameraPos;
	CamVector m_CameraAt;
	CamVector m_CameraUp;

	void SetCamera();

	bool MoveCamera(GLfloat _dt);
	void RotateScene(GLfloat _dt);

	void Update(GLfloat _dt);

	bool rotateCam;
	bool rotateToStart;

private:
	static const GLuint m_interval = 20;
	GLuint m_CameraCount;
};