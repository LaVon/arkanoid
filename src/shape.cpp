#include "shapes.h"

bool Shape::LoadShapeTexture(char* _image)
{
	bool res = true;
	AUX_RGBImageRec* currTexture;
	currTexture = auxDIBImageLoad(_image);
	if(!currTexture)
		res = false;
	glGenTextures(1, &m_shapeTexture);
	glBindTexture(GL_TEXTURE_2D, m_shapeTexture);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, currTexture->sizeX, currTexture->sizeY, 0, GL_RGB, GL_UNSIGNED_BYTE, currTexture->data);
	delete currTexture;
	return res;
}

