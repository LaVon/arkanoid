#include "shapes.h"

#define MOVE_LEFT 0
#define MOVE_RIGHT 1
#define MOVE_VALUE 0.02

class Board : public Cube
{
public:
	Board();
	~Board();
	void Restart();
	void DrawBoard();
	void MoveBoard(float _dt, int _side);
	void setZeroDiffrence();

	//���������� � ��������, ��� ������������ ��������� ������ � ������ �������� �������� �� ����
	GLfloat m_previous_x;
	//������� ����� ������������ ����������� � ������� ��������
	GLfloat m_diff_board;	
};