#include "board.h"

Board::Board()
{
	a = 1.0;
	b = 1.0;
	c = 1.0;

	A[0] = a/2; A[1] = b/2; A[2] = c/2;
	B[0] = -a/2; B[1] = b/2; B[2] = c/2;
	C[0] = -a/2; C[1] = -b/2; C[2] = c/2;
	D[0] = a/2; D[1] = -b/2; D[2] = c/2;
	E[0] = a/2; E[1] = b/2; E[2] = -c/2;
	F[0] = -a/2; F[1] = b/2; F[2] = -c/2;
	G[0] = -a/2; G[1] = -b/2; G[2] = -c/2;
	H[0] = a/2; H[1] = -b/2; H[2] = -c/2;
	
	Restart();
}

void Board::Restart()
{
	x = 14;
	y = 0;
	z = -7;

	m_previous_x = 0;
	m_diff_board = 0;
}

Board::~Board()
{

}
void Board::DrawBoard()
{
	glPushMatrix();
	glColor3f(1.0, 1.0, 1.0);
	glTranslatef(x, y, z);
	glScalef(4.0, 1.0, 1.0);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, m_shapeTexture);
	glBegin(GL_QUADS);
	
	//Front face
	glNormal3f( 0.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3fv(A);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3fv(B);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3fv(C);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3fv(D);
	
	//Back face
	glNormal3f( 0.0f, 0.0f,-1.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3fv(F);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3fv(E);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3fv(H);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3fv(G);
	
	//Right face
	glNormal3f( 1.0f, 0.0f, 0.0f);
	glTexCoord2f(0.0f, 1.0f);
    glVertex3fv(E);
	glTexCoord2f(0.0f, 0.0f);
    glVertex3fv(A);
	glTexCoord2f(1.0f, 0.0f);
    glVertex3fv(D);
	glTexCoord2f(1.0f, 1.0f);
    glVertex3fv(H);
	
	//Left face
	glNormal3f(-1.0f, 0.0f, 0.0f);
	glTexCoord2f(1.0f, 1.0f);
    glVertex3fv(B);
	glTexCoord2f(0.0f, 1.0f);
    glVertex3fv(F);
	glTexCoord2f(0.0f, 0.0f);
    glVertex3fv(G);
	glTexCoord2f(1.0f, 0.0f);
    glVertex3fv(C);
	
	//Top face
	glNormal3f( 0.0f, 1.0f, 0.0f);
	glTexCoord2f(1.0f, 0.0f);
    glVertex3fv(E);
	glTexCoord2f(1.0f, 1.0f);
    glVertex3fv(F);
	glTexCoord2f(0.0f, 1.0f);
    glVertex3fv(B);
	glTexCoord2f(0.0f, 0.0f);
    glVertex3fv(A);
	
	//Bottom face
	glNormal3f( 0.0f, -1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
    glVertex3fv(D);
	glTexCoord2f(1.0f, 0.0f);
    glVertex3fv(C);
	glTexCoord2f(1.0f, 1.0f);
    glVertex3fv(G);
	glTexCoord2f(0.0f, 1.0f);
    glVertex3fv(H);
	glEnd();
	
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
}

void Board::MoveBoard(float _dt, int _side)
{
	if (_side == MOVE_LEFT && x > BORDER_LEFT)
	{
	    x -= MOVE_VALUE * _dt;
		
	}
	else if (_side == MOVE_RIGHT && x < BORDER_RIGHT - a)
		x += MOVE_VALUE * _dt;
}

void Board::setZeroDiffrence()
{
	m_diff_board = 0;
	m_previous_x = 0;
}
