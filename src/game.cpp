#include "game.h"
#include <stdio.h>
#include <math.h>
#include <iostream>

using namespace std;

LRESULT	CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

Game* Game::m_pThis = 0;
int Game::m_iRefCount = 0;

Game::Game(): hDC(0), hRC(0), hWnd(0)
{
	m_bIsVirtual = false;
	memset(m_abKeys, 0, sizeof(m_abKeys));
	//���������� ���� ����������
	m_bActive = true;
	//������� �� ����
	m_bPlay = false;
	m_bLost = false;
	m_bWin = false;
	m_Life = 3;
	//m_bFullscreen = true;

	//������� ����
	LightAmbient[0] = 0.3;
	LightAmbient[1] = 0.3;
	LightAmbient[2] = 0.3;
	LightAmbient[3] = 0.3;

	//���� ����� ���������
	LightDiffuse[0] = 1.0;
	LightDiffuse[1] = 1.0;
	LightDiffuse[2] = 1.0;
	LightDiffuse[3] = 1.0;

	//������� �����
	LightPosition[0] = 0.0;
	LightPosition[1] = 0.0;
	LightPosition[2] = 2.0;
	LightPosition[3] = 1.0;

}

Game::~Game()
{

}

Game* Game::getInstance()
{
	if (m_pThis == 0)
		m_pThis = new Game();
	m_iRefCount++;

	return m_pThis;
}

void Game::Release()
{
	if (m_pThis == 0)
		delete this;
	else 
		m_iRefCount --;
}

LRESULT CALLBACK WndProc(//���������� ������� ���� 
						HWND hWnd, 
						//��������� ��� ����� ����
						UINT uMsg, 
						//�������������� ���������
						WPARAM wParam, 
						LPARAM lParam)
{
	Game* pWin = Game::getInstance();
	switch (uMsg)
	{
		case WM_ACTIVATE:
		{
			//�������� ��������� �����������
			if (!HIWORD(wParam))
				pWin->m_bActive = true;
			else 
				pWin->m_bActive = false;
			return 0;
		}
		case WM_CLOSE:
		{
			PostQuitMessage(0);
			return 0;
		}
		case WM_KEYDOWN:
		{
				pWin->m_abKeys[wParam] = true;
				return 0;
		}
		case WM_KEYUP:
		{
				pWin->m_abKeys[wParam] = false;
				return 0;
		}
		case WM_SIZE:
		{
			pWin->ReDrawScene(LOWORD(lParam), HIWORD(lParam));
			return 0;
		}
	}
	//delete pWin;
	//�������������� ��������� �� ��������� Windows
	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

bool Game::InitGame()
{
	if(!m_board.LoadShapeTexture("board.bmp")
	   || !m_ball.LoadShapeTexture("ballMetall.bmp"))
		return false;
	for (int i = 0; i < m_wall.wall_size; i++)
	{
		if(!m_wall.m_brick[i].LoadShapeTexture("stone-texture.bmp"))
			return false;
	}
	//������� �������� �����������
	glShadeModel(GL_SMOOTH);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	//����� �������
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	//��������� � ���������� �����������
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	
	glLightfv(GL_LIGHT1, GL_AMBIENT, LightAmbient);		// Setup The Ambient Light
	glLightfv(GL_LIGHT1, GL_DIFFUSE, LightDiffuse);		// Setup The Diffuse Light
	glLightfv(GL_LIGHT1, GL_POSITION,LightPosition);	// Position The Light
	glEnable(GL_LIGHT1);
	//BuildFont();
	return true;
}

bool Game::CreateGLWindow(char* title, int width, int height, int bits, bool fullscreen)
{
	//������ ��������
	GLuint	PixelFormat;
	WNDCLASS window;
	//����������� ����� ����
	DWORD dwExStyle; 
	//������� ����� ����
	DWORD dwStyle;
	RECT windowRect;
	windowRect.left = (long)0;
	windowRect.right = (long)width;
	windowRect.top = (long)0;
	windowRect.bottom = (long)height;

	m_bFullscreen = fullscreen;

	//������� ���������� (���������) ����������.
	hInstance = GetModuleHandle(0);
	/*
		����� CS_HREDRAW � CS_VREDRAW ���������� ������������ ���� ������ ���, ����� ��� ������������. 
		CS_OWNDC ������� ������� DC ��� ����. 
		��� ��������, ��� DC �� ������������ ��������� ����������� ������������.
	*/
	window.style	= CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	//WndProc - ���������, ������� ������������� ��������� ��� ���������.
	window.lpfnWndProc	=  WndProc;	
	//�������������� ���������� ��� ���� �����������
	window.cbClsExtra = 0;
	window.cbWndExtra = 0;
	//���������� ���������� ����������
	window.hInstance = hInstance;
	//������ ����������
	window.hIcon = LoadIcon(0, IDI_WINLOGO);
	//��������� �����
	window.hCursor = LoadCursor(0, IDC_ARROW);
	//���
	window.hbrBackground = 0;
	//����
	window.lpszMenuName = 0;
	//��� ������
	window.lpszClassName = "Arkanoid";
	//ShowCursor(false);
	//������� ���������������� ����� ����
	if (!RegisterClass(&window))
	{
		MessageBox(0, "Failed To Register The Window Class.", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return false;
	}
	//���� ������������� �����
	/*if (fullscreen)												
	{
		//����� ����������
		DEVMODE dmScreenSettings;
		//������� ��� �������� ���������
		memset(&dmScreenSettings,0,sizeof(dmScreenSettings));

		dmScreenSettings.dmSize=sizeof(dmScreenSettings);		
		dmScreenSettings.dmPelsWidth	= width;				
		dmScreenSettings.dmPelsHeight	= height;				
		dmScreenSettings.dmBitsPerPel	= bits;					
		dmScreenSettings.dmFields=DM_BITSPERPEL|DM_PELSWIDTH|DM_PELSHEIGHT;

		if (ChangeDisplaySettings(&dmScreenSettings,CDS_FULLSCREEN)!=DISP_CHANGE_SUCCESSFUL)
		{
			if (MessageBox(0,"The Requested Fullscreen Mode Is Not Supported By\nYour Video Card. Use Windowed Mode Instead?","NeHe GL",MB_YESNO|MB_ICONEXCLAMATION)==IDYES)
			{
				fullscreen = false;		
			}
			else
			{
				MessageBox(0,"Program Will Now Close.","ERROR",MB_OK|MB_ICONSTOP);
				return false;									
			}
		}
	}

	if (fullscreen)												
	{
		dwExStyle=WS_EX_APPWINDOW;								
		dwStyle=WS_POPUP;										
		ShowCursor(FALSE);										
	}
	else*/
	{
		dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;			
		dwStyle = WS_OVERLAPPED|WS_CAPTION|WS_SYSMENU|WS_MINIMIZEBOX;							
	}
	/*
		��������� ������� AdjustWindowRectEx �� ���������, ����� �� ���� ����� ���� OpenGL �� ���� ��������� ���������, 
		������ ����� ����� ������� �������������� ������� ���������� ��� ����� ������. 
	*/
	//AdjustWindowRectEx(&windowRect, dwStyle, false, dwExStyle);
	//��������� ����
	if (!(hWnd = CreateWindowEx(dwExStyle,
								"Arkanoid",
								title,
								//����� WS_CLIPSIBLINGS � WS_CLIPCHILDREN �� ��������� ������ ����� ���������� ������ ��� ������ ������ OpenGL ����
								WS_CLIPSIBLINGS |
								WS_CLIPCHILDREN |
								dwStyle,
								//������� ����
								0, 0, 
								//������ � ������ 
								windowRect.right - windowRect.left,
								windowRect.bottom - windowRect.top,
								//��� ������������� ����
								0,
								//��� ����
								0,
								hInstance,
								0)))
	{
		ShowError("Window creation error.");
		return false;
	}
	//�������� ������� �������� (Pixel Format)
	//���������� static ��������� ���� ��������� ����� �������� � �������, � ������� ��� ����������
	//pfd �������� Windows ����� ����� ����� �� ����� ������� �������
	static PIXELFORMATDESCRIPTOR pfd =
	{
		//������ ����������� ������� ������� ��������
		sizeof(PIXELFORMATDESCRIPTOR), 
		//������
		1, 
		PFD_DRAW_TO_WINDOW |
		PFD_SUPPORT_OPENGL |
		PFD_DOUBLEBUFFER,
		PFD_TYPE_RGBA,
		//��� ������� �����
		bits,
		//�������� ���� (���)
		0, 0, 0, 0, 0, 0,
		//����� ������������ (���)
		0,
		//��������� ��� (���)
		0,
		//����� ���������� (���)
		0,
		//���� ���������� (���)
		0, 0, 0, 0,
		//����� �������
		32,
		//����� ��������� (���)
		0,
		//��������������� ����� (���)
		0,
		//������� ���� ��������� 
		PFD_MAIN_PLANE,
		//���������������
		0,
		//����� ���� (���)
		0, 0, 0
	};
	//������� �������� �������� ���������� 
	if (!(hDC = GetDC(hWnd)))
	{
		ShowError("Can't create a GL device context.");
		return false;
	}
	//����� ������� ��������, ���������� ����
	if (!(PixelFormat = ChoosePixelFormat(hDC, &pfd)))
	{
		ShowError("Can't Find A Suitable PixelFormat.");
		return false;
	}
	//��������� ������� ��������
	if (!SetPixelFormat(hDC, PixelFormat, &pfd))
	{
		ShowError("Can't Set The PixelFormat.");
		return false;
	}
	//������� �������� �������� ����������
	if (!(hRC = wglCreateContext(hDC)))
	{
		ShowError("Can't Create A GL Rendering Context.");
		return false;
	}
	//������� �������� ���������� ��������
	if (!wglMakeCurrent(hDC, hRC))
	{
		ShowError("Can't Activate The GL Rendering Context.");
		return false;
	}
	//�������� ����
	ShowWindow(hWnd, SW_SHOW);
	//���� ������� �� �������� ����
	SetForegroundWindow(hWnd);
	//����� ����� �� ����
	SetFocus(hWnd);
	/*
		����� ReDrawScene, ��������� ������ � ������ ������ ��� ��������� ����������� �������� OpenGL ������.
	*/
	ReDrawScene(width, height);
	/*
		� InitGL() ����� ��������� ���������, �������� ... 
	*/
	if (!InitGame())
	{
		ShowError("Initialization game failed.");
		return false;
	}
	
	//m_pFrame = new GLFrame();
	
	return true;
}

void Game::ShowError(char* text)
{
	KillGLWindow();
	MessageBox( 0, text, "ERROR", MB_OK | MB_ICONEXCLAMATION );
}

void Game::ReDrawScene(GLsizei width, GLsizei height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, width, height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

bool Game::DrawScene()
{
	//������� ������ � ������ �������
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//����� ������� �������
	glLoadIdentity();
	m_camera.SetCamera();
	glEnable(GL_LIGHTING);
	m_wall.DrawWall();
	m_ball.DrawBall();
	m_board.DrawBoard();
	return true;
}

void Game::Win()
{
	MessageBox(NULL, "You win :)", "Congratulations", MB_OK | MB_ICONEXCLAMATION);
	//m_sound.PlayMySound("explosion.wav");
	m_wall.m_countDestroyed = 0;
	m_Life = LIFE_COUNT;
}

void Game::Lost()
{
	MessageBox(NULL, "You lost :(", "Sadness", MB_OK | MB_ICONEXCLAMATION);
	//m_sound.PlayMySound("explosion.wav");
	m_wall.m_countDestroyed = 0;
	m_Life = LIFE_COUNT;
}

void Game::DestroyBrick(Brick* _brick)
{
	_brick->isAlive = false;
	m_wall.m_countDestroyed++;
	m_sound.PlayMySound("explosion.wav");
	if (m_wall.m_countDestroyed == m_wall.wall_size)
	{
		m_bWin = true;
		m_bPlay = false;
	}
}

bool Game::CheckBrickCollision()
{
	for (int i = 0; i < m_wall.wall_size; i++)
	{
		if (m_wall.m_brick[i].isAlive)
		{
			//���� �� � � ���� �������
			if (m_ball.x + m_ball.radius > m_wall.m_brick[i].x - m_wall.m_brick[i].a/2
				&& m_ball.x - m_ball.radius < m_wall.m_brick[i].x + m_wall.m_brick[i].a/2)
			{
				//���� �� y � ���� ������� - ������ ������ ������ ��� ����� - ������ �� y
				if(m_ball.y + m_ball.radius > m_wall.m_brick[i].y - m_wall.m_brick[i].b/2
				   && m_ball.y - m_ball.radius < m_wall.m_brick[i].y + m_wall.m_brick[i].b/2)
				{
					m_ball.ChangeYDir();
					DestroyBrick(&m_wall.m_brick[i]);

					return true;
				} 
			}
			//�� � � ���� �������
			if(m_ball.y + m_ball.radius > m_wall.m_brick[i].y - m_wall.m_brick[i].b/2
				    && m_ball.y - m_ball.radius < m_wall.m_brick[i].y + m_wall.m_brick[i].b/2)
			{
				//�� x ���� �� ������� ���� ������� �� �������� ������� ��� ����� ��� �������� �� ������ ���� �� ��������
				if((m_ball.x - m_ball.radius > m_wall.m_brick[i].x + m_wall.m_brick[i].a/2
				   && m_ball.x - m_ball.radius < m_wall.m_brick[i].x + m_wall.m_brick[i].a)
				   || (m_ball.x + m_ball.radius < m_wall.m_brick[i].x - m_wall.m_brick[i].a/2 
				   && m_ball.x + m_ball.radius > m_wall.m_brick[i].x - m_wall.m_brick[i].a))
				{
					m_ball.ChangeXDir();
					DestroyBrick(&m_wall.m_brick[i]);

					return true;
				}
			}
		}
	}
}

bool Game::CheckBoardCollision()
{
	GLfloat persent = 40.0;
	GLfloat limitSpeed = 0.02;
	if (m_ball.x + m_ball.radius > m_board.x - m_board.a && m_ball.x - m_ball.radius < m_board.x + m_board.a
		&& m_ball.y < m_board.y + m_board.b && m_ball.y > m_board.y/* + m_board.b/2*/
		&& !m_ball.isUp)
	{
		if (m_board.m_previous_x != m_board.x && m_bIsVirtual)
		{
			m_board.m_diff_board = m_board.x - m_board.m_previous_x;
			m_ball.m_fSpeed += fabs(m_board.m_diff_board/persent);
			m_bIsVirtual = false;
			if (m_ball.m_fSpeed > limitSpeed)
				m_ball.m_fSpeed = limitSpeed;
		}
		
		m_sound.PlayMySound("boom.wav");
		
		m_ball.ChangeYDir();
		return true;
	}
}

bool Game::CheckBorderCollision()
{
	//����
	if (m_ball.x < BORDER_LEFT && m_ball.isLeft)
	{
		m_ball.ChangeXDir();
		return true;
	}
	//�����
	if (m_ball.x > BORDER_RIGHT - m_ball.radius && m_ball.isRight)
	{
		m_ball.ChangeXDir();
		return true;
	}
	//�������
	if (m_ball.y > BORDER_TOP - m_ball.radius && m_ball.isUp)
	{
		m_ball.ChangeYDir();
		return true;
	}
	//���
	if (m_ball.y < BORDER_BOTTOM - BALL_END)
	{
		//Restart();
		m_ball.FallBall();
		m_bPlay = false;
		m_bLost = true;
		return false;
	}
}

bool Game::CheckCollision()
{
	bool res = false;

	//�������
	res = CheckBrickCollision();

	//����������� ��������
	//���������� ���������� ��������, ������� ��������� ��� �������� ��������� �� ����� ����������� �� ��� ������
	//��� �� ����� ����� ������������ � �������� ��������, �� ����� �������� ����������, � ���� �������� ���������� - ������ ���� ���������.
	GLfloat y1 = 0.5;
	GLfloat y2 = 0.4;
	if (m_ball.y < m_board.y + m_board.b + y1 && m_ball.y > m_board.y + m_board.b + y2 && !m_ball.isUp)
	{
		m_board.m_previous_x = m_board.x;
		m_bIsVirtual = true;
		return false;
	}

	//��������
	res = CheckBoardCollision();

	//������� ����
	res = CheckBorderCollision();

	return res;
}

void Game::CheckBeat(bool _isCollision)
{
	//������ �������� ����� ������� ������������ � ������������� ����� ��������� ������
	if (m_ball.m_fSpeed > m_ball.m_fStartSpeed && _isCollision && m_board.m_diff_board == 0)
	{
		if (m_ball.m_fSpeed < 2 * m_ball.m_fStartSpeed)
		    m_ball.m_fSpeed = m_ball.m_fStartSpeed;
		else
			m_ball.m_fSpeed -= m_ball.m_fStartSpeed;
	}
	//���������, �������� ������� � �������, � ������� ������ ������ ��� ����� ������������ � ��� ��� � ���������������
	//���� � �� �� �������, �� ������ ���� �����������, ��� ������, ����� ����� ����������� � ����������  � �� �� �������, ������ �����
	if (m_board.m_diff_board > 0)
		{
			if (m_ball.isLeft)
			    m_ball.ChangeXDir();
			m_board.setZeroDiffrence();
		}
	else if (m_board.m_diff_board < 0)
		{
			if (m_ball.isRight)
			    m_ball.ChangeXDir();
			m_board.setZeroDiffrence();
		}
}

bool Game::Update(GLfloat _dt)
{
	Game* pWin = Game::getInstance();
	if (m_bActive)
	{
		DrawScene();
		m_camera.Update(_dt);
		SwapBuffers(hDC);
	
		if (pWin->m_abKeys[VK_Z])
			m_camera.rotateCam = true;

		if (m_bPlay)
		{
			bool res = CheckCollision();
			CheckBeat(res);
			m_ball.MoveBall(_dt);

			if (pWin->m_abKeys[VK_LEFT])
				m_board.MoveBoard(_dt, MOVE_LEFT);
				
			if (pWin->m_abKeys[VK_RIGHT])
				m_board.MoveBoard(_dt, MOVE_RIGHT);
				
		}

		else 
			{
				if (m_bLost)
				{
					if (!m_camera.MoveCamera(_dt))
						 Restart();
				}
				else if (m_bWin)
					     Restart();
				else 
				{
					if (pWin->m_abKeys[VK_SPACE])
						m_bPlay = true;
				}
			}
		
		
	}
	if (pWin->m_abKeys[VK_ESCAPE])
		return true;
	/*if (pWin.m_abKeys[VK_F1])					
			{
				pWin.m_abKeys[VK_F1] = false;
				KillGLWindow();						
				m_bFullscreen =! m_bFullscreen;
				CreateGLWindow("Arkanoid",1024,768,16, m_bFullscreen);
			}*/
	return false;
}

void Game::Restart()

{
	m_board.Restart();
	m_ball.Restart();
	
	if (m_Life == 1 || m_bWin)
	{
		if (m_bLost)
			Lost();
		if (m_bWin)
			Win();
		m_wall.Restart();
	}
	else
		m_Life--;

	if (m_bWin)	
	    m_bWin = false;
	if (m_bLost)
		m_bLost = false;

	m_bIsVirtual = false;
}

void Game::KillGLWindow()
{
	//���� fullscreen
	//...
	if (hRC)
	{
		//�������� �� ���������� RC & DC
		if (!wglMakeCurrent(0,0))
			MessageBox(0, "Release Of DC And RC Failed.", "SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);

		//�������� �� ������� RC
		if (!wglDeleteContext(hRC))
			MessageBox(0, "Release Rendering Context Failed.", "SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);
		
		hRC = 0;
	}
	//�������� �� ���������� �������� ����������
	if (hDC && !ReleaseDC(hWnd, hDC))
	{
		MessageBox(0, "Release Device Context Failed.", "SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);
        hDC = 0;
	}
	if (hWnd &&  !DestroyWindow(hWnd))
	{
		MessageBox(0, "Could Not Release hWnd.", "SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);
        hWnd = 0;         
	}
	// �������� �� ����������������� �����
	if( !UnregisterClass("Arkanoid", hInstance))      
    {
        MessageBox(0, "Could Not Unregister Class.", "SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);
        hInstance = 0;
    }
}