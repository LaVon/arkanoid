#include "sound.h"

Sound::Sound()
{	
	LoadSound();	
	InitSound();
}

Sound::~Sound()
{

}

void Sound::LoadSound()
{
	//������� � ����� ��������
	trackSound["explosion.wav"] = brickBoom;
	trackSound["boom.wav"] = boardTick;
}

bool Sound::CheckError(FMOD_RESULT _result)	
{						
	if (_result != FMOD_OK)
	{
		printf("FMOD error! (%d) %s\n", _result, FMOD_ErrorString(_result));
		return false;
	}
	return true;
}

void Sound::InitSound()
{
	result = FMOD::System_Create(&fmodSystem); 
	CheckError(result);
	
	result = fmodSystem->init(16, FMOD_INIT_NORMAL, 0);
	CheckError(result);

	//Create stream
	CreateStream();
}

void Sound::CreateStream()
{
	map<string, FMOD::Sound*>::iterator it;
	for (it = trackSound.begin(); it != trackSound.end(); it++)
	{
		result = fmodSystem->createStream(it->first.c_str(), FMOD_SOFTWARE | FMOD_LOOP_OFF, 0, &(it->second));
		CheckError(result);
	}
}

void Sound::PlayMySound(char* _track)
{
	map<string, FMOD::Sound*>::iterator it;
	it = trackSound.find(_track);
	if (it != trackSound.end())
	{
		if (strcmp(it->first.c_str(), _track) == 0)
		{
			result = fmodSystem->playSound(it->second, 0, false, &soundChannel);
			CheckError(result);
		}
	}
}

void Sound::Update()
{
	fmodSystem->update();
}