#include "wall.h"

Brick::Brick()
{
	a = 1.0;
	b = 1.0;
	c = 1.0;

	A[0] = a/2; A[1] = b/2; A[2] = c/2;
	B[0] = -a/2; B[1] = b/2; B[2] = c/2;
	C[0] = -a/2; C[1] = -b/2; C[2] = c/2;
	D[0] = a/2; D[1] = -b/2; D[2] = c/2;
	E[0] = a/2; E[1] = b/2; E[2] = -c/2;
	F[0] = -a/2; F[1] = b/2; F[2] = -c/2;
	G[0] = -a/2; G[1] = -b/2; G[2] = -c/2;
	H[0] = a/2; H[1] = -b/2; H[2] = -c/2;
}

Brick::~Brick()
{

}

Wall::Wall()
{
	wall_size = 60;
	m_brick = new Brick[wall_size];
	BuildWall();
	m_countDestroyed = 0;
}

void Wall::BuildWall()
{	
	//���������� �������� ������� �����
	GLfloat curr_x = 2.7;
	GLfloat curr_y = 19.0;
	GLfloat curr_z = -7;
	//���������� �������� � ���� �����
	GLint count = 10;
	//������� �������� � ����
	GLint iter = 1;
	GLfloat offsetX = 1.5;
	GLfloat offsetY = 1.5;
	GLfloat firstOffsetX = 0.2;
	for (int i = 0; i < wall_size; i++)
	{
		m_brick[i].isAlive = true;
		//��������� �� ����� ���
		if (iter > count)
		{
			curr_y -= offsetY * m_brick[i].b;
			curr_x = m_brick[i - count].x;
			//����� ������� � ����
			iter = 1;
		}
		m_brick[i].x = curr_x;
		m_brick[i].y = curr_y;
		m_brick[i].z = -7;
		curr_x += m_brick[i].a + offsetX; 
		iter ++;
	}
}

void Wall::Restart()
{
	for (int i = 0; i < wall_size; i++)
		m_brick[i].isAlive = true;
}

Wall::~Wall()
{
	delete[] m_brick;
}

void Wall::DrawWall()
{
	for (int i = 0; i < wall_size; i++)
	{
		glPushMatrix();
		
		if (!m_brick[i].isAlive)
		{
			glPopMatrix();
			continue;
		}
		glColor3f(1.0,1.0,1.0);
		glTranslatef(m_brick[i].x, m_brick[i].y, m_brick[i].z);
		glScalef(2.0, 1.0, 1.0);
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, m_brick[i].m_shapeTexture);
		glBegin(GL_QUADS);
		
		//Front face
	    glNormal3f( 0.0f, 0.0f, 1.0f);
		glTexCoord2f(0.0f, 0.0f);
		glVertex3fv(m_brick[i].A);
		glTexCoord2f(1.0f, 0.0f);
		glVertex3fv(m_brick[i].B);
		glTexCoord2f(1.0f, 1.0f);
		glVertex3fv(m_brick[i].C);
		glTexCoord2f(0.0f, 1.0f);
		glVertex3fv(m_brick[i].D);
		
		//Back face
	    glNormal3f( 0.0f, 0.0f,-1.0f);
		glTexCoord2f(1.0f, 0.0f);
		glVertex3fv(m_brick[i].F);
		glTexCoord2f(1.0f, 1.0f);
		glVertex3fv(m_brick[i].E);
		glTexCoord2f(0.0f, 1.0f);
		glVertex3fv(m_brick[i].H);
		glTexCoord2f(0.0f, 0.0f);
		glVertex3fv(m_brick[i].G);
		
		//Right face
	    glNormal3f( 1.0f, 0.0f, 0.0f);
		glTexCoord2f(0.0f, 1.0f);
		glVertex3fv(m_brick[i].E);
		glTexCoord2f(0.0f, 0.0f);
		glVertex3fv(m_brick[i].A);
		glTexCoord2f(1.0f, 0.0f);
		glVertex3fv(m_brick[i].D);
		glTexCoord2f(1.0f, 1.0f);
		glVertex3fv(m_brick[i].H);
		
		//Left face
	    glNormal3f(-1.0f, 0.0f, 0.0f);
		glTexCoord2f(1.0f, 1.0f);
		glVertex3fv(m_brick[i].B);
		glTexCoord2f(0.0f, 1.0f);
		glVertex3fv(m_brick[i].F);
		glTexCoord2f(0.0f, 0.0f);
		glVertex3fv(m_brick[i].G);
		glTexCoord2f(1.0f, 0.0f);
		glVertex3fv(m_brick[i].C);
		
		//Top face
	    glNormal3f( 0.0f, 1.0f, 0.0f);
		glTexCoord2f(1.0f, 0.0f);
		glVertex3fv(m_brick[i].E);
		glTexCoord2f(1.0f, 1.0f);
		glVertex3fv(m_brick[i].F);
		glTexCoord2f(0.0f, 1.0f);
		glVertex3fv(m_brick[i].B);
		glTexCoord2f(0.0f, 0.0f);
		glVertex3fv(m_brick[i].A);
		
		//Bottom face
	    glNormal3f( 0.0f, -1.0f, 0.0f);
		glTexCoord2f(0.0f, 0.0f);
		glVertex3fv(m_brick[i].D);
		glTexCoord2f(1.0f, 0.0f);
		glVertex3fv(m_brick[i].C);
		glTexCoord2f(1.0f, 1.0f);
		glVertex3fv(m_brick[i].G);
		glTexCoord2f(0.0f, 1.0f);
		glVertex3fv(m_brick[i].H);
		glEnd();
		glDisable(GL_TEXTURE_2D);
		glPopMatrix();
	}
}