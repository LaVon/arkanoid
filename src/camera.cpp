#include "camera.h"

#define ROTATE_Y -13.7
#define ROTATE_Z 16.0

#define START_Y 10
#define START_Z 20

#define ROTATE_Y_STEP 0.035
#define ROTATE_Z_STEP 0.005

Camera::Camera()
{
	m_CameraPos.x = 14.0;
	m_CameraPos.y = 10.0;
	m_CameraPos.z = 20.0;

	m_CameraAt.x = 14.0;
	m_CameraAt.y = 10.0;
	m_CameraAt.z = -7.0;

	m_CameraUp.x = 0.0;
	m_CameraUp.y = 1.0;
	m_CameraUp.z = 0.0;

	m_CameraCount = 1;

	rotateCam = false;
	rotateToStart = false;
}

void Camera::SetCamera()
{
	gluLookAt(m_CameraPos.x, m_CameraPos.y, m_CameraPos.z, //�����
			  m_CameraAt.x, m_CameraAt.y, m_CameraAt.z, //������ �� (����)
			  m_CameraUp.x, m_CameraUp.y, m_CameraUp.z);
}

bool Camera::MoveCamera(GLfloat _dt)
{
	GLfloat moveX = 0.02; 
	GLfloat moveY = 0.02;
	GLfloat moveZ = 0.02;
	
	CamVector moveVector = CamVector(moveX * _dt, moveY * _dt, moveZ * _dt);

	if (m_CameraCount < 180 && m_CameraCount > 0)
		m_CameraCount ++;
	else 
	{
		m_CameraCount = 1;
		return false;
	}
	switch(m_CameraCount)
	{
		case m_interval:
		case 4 * m_interval:
		case 7 * m_interval:
			{
				m_CameraAt -= moveVector;
				break;
			}
		case 2 * m_interval:
		case 5 * m_interval:
		case 9 * m_interval:
			{
				moveVector *= 2.0;
				m_CameraAt += moveVector;
				break;
			}
		case 3 * m_interval:
		case 6 * m_interval:
		case 8 * m_interval:
			{
				m_CameraAt -= moveVector;
				break;
			}
	}
	return true;
}

void Camera::Update(GLfloat _dt)
{
	if (rotateCam)
		RotateScene(_dt);
}

void Camera::RotateScene(GLfloat _dt)
{
	if (m_CameraPos.y > ROTATE_Y && !rotateToStart)
	{
		m_CameraPos.y -= ROTATE_Y_STEP  * _dt;
		m_CameraPos.z -= ROTATE_Z_STEP * _dt;
	}
	else if (m_CameraPos.y < START_Y && rotateToStart)
	{
		m_CameraPos.y += ROTATE_Y_STEP  * _dt;
		m_CameraPos.z += ROTATE_Z_STEP * _dt;
	}
	else
	{
		rotateCam = false;
		rotateToStart = !rotateToStart;
	}
}