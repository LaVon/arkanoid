#include <windows.h>
#include "game.h"
#include "timer.h"

int  WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	bool exit = false;
	MSG msg;
	Game* pGame = Game::getInstance();
	
	if (!pGame->CreateGLWindow("Arkanoid", 800, 600, 32, false))
	{
		delete pGame;
		return 0;
	}
	GLFrame* m_pFrame = new GLFrame();
	while (!exit)
	{
		float dt = m_pFrame->Update();
		if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				exit = true;
			else 
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else		
		{
			exit = pGame->Update(dt);
		}
	}
	pGame->KillGLWindow();
	pGame->Release();
	return 0;
}