#include <math.h>
#include <windows.h>
#include <gl\glaux.h>
#include <gl\glut.h>
#include "ball.h"
#include "wall.h"
#include "camera.h"
#include "sound.h"

#define VK_Z 0x5A
#define LIFE_COUNT 3

class Game
{
public:
	static Game* getInstance();
	void Release();
	Game();
	~Game();

	bool CreateGLWindow(char* title, int width, int height, int bits, bool fullscreen);
	bool Update(GLfloat _dt);
	//��������� ����������� �������� OpenGL ������
	void ReDrawScene(GLsizei width, GLsizei height);
	void KillGLWindow();
	//���������� ����
	bool m_bActive;
	bool m_abKeys[256];
	//���������� ����
	bool m_bPlay;
	bool	m_bFullscreen;

	Camera m_camera;
	Sound m_sound;
	Board m_board;
	Ball m_ball;
	Wall m_wall;

private:
	bool InitGame();
	bool DrawScene();
	void ShowError(char* text);
	bool CheckCollision();
	void CheckBeat(bool _isCollision);
	void Restart();

	bool CheckBrickCollision();
	void DestroyBrick(Brick* _brick);
	bool CheckBoardCollision();
	bool CheckBorderCollision();

	void Lost();
	void Win();

	static Game* m_pThis;
	static int m_iRefCount;
	//������� �� ������ ���������� ����������� ��������
	bool m_bIsVirtual;
	GLuint m_Life;
	GLuint m_baseList;
	/*
		������ OpenGL ��������� ����������� � ���������� ����������, ������� � ���� ������� �������� �������� ���������� (Device Context). 
		�������� ���������� OpenGL ��������� ��� hRC. 
		��� ���� ����� �������� � ����, ��� ���������� ������� �������� ���������� Windows, ������� ��������� ��� hDC. 
		DC ��������� ���� � GDI (Graphics Device Interface). 
		RC ��������� OpenGL � DC.
	*/
	//�������� ����������
	HDC	hDC;
	//�������� ����������
	HGLRC	hRC;
	//���������� (���������� ������-�������������) ���� 
	HWND	hWnd;
	//���������� ���������� - (���������) ���������
	HINSTANCE	hInstance;

	GLfloat LightAmbient[4];
	GLfloat LightDiffuse[4];
	GLfloat LightPosition[4];

	bool  m_bLost;
	bool  m_bWin;
};