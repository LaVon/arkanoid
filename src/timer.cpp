#include "timer.h"

GLTimer::GLTimer()
{
	__int64 frequency;	// The performance counter frequency

	// check for performance counter
	if(QueryPerformanceFrequency((LARGE_INTEGER *) &frequency))
	{
		// Use the performance counter to get the present time
		QueryPerformanceCounter((LARGE_INTEGER *) &m_iPerfStart);
		// say this computer has a performance counter
		m_bPerformance = true;
		// calculate the resolution from the frequency
		m_bResolution = (float) ( ((double)1.0f)/((double)frequency) );
	}
	else
	{	
		// No performance counter available
		m_bPerformance = false;
		// Get the present time
		m_dwStart = GetTickCount() / 1000; //timeGetTime();
		// calculate the resolution
		m_bResolution = 1.0f/1000.0f;
	}
}

float GLTimer::GetGLTime()
{
	// get the time from the available counter
	if(m_bPerformance)
	{
		__int64 time;			// the current time

		// get the time
		QueryPerformanceCounter((LARGE_INTEGER *) &time);
		// calculate it in milliseconds
		return ( (float)(time-m_iPerfStart)*m_bResolution )*1000.0f;
	}else
		//return ( (float)(timeGetTime()-mm_start)*resolution )*1000.0f;
		return ( (float)((GetTickCount()/1000)-m_dwStart)*m_bResolution )*1000.0f;
}

//
//	Frame class
//
GLFrame::GLFrame()
{
	// allocate a new timer
	m_pTimer = new GLTimer();
	// get the present time
	m_fLastTime = m_pTimer->GetGLTime();
	// clear everything else
	m_iFps = 0;
	m_fLength = 0.0f;
}

float GLFrame::Update()
{
	// get the the present time
	float now = m_pTimer->GetGLTime();

	// calculate the length of the last frame
	m_fLength = now - m_fLastTime;
	// make sure it's sensible
	if(m_fLength <= 0.0f)
		m_fLength = 1.0f;

	// and the frames per second
	m_iFps = (int)(1000.0f/m_fLength);

	// and save the last time
	m_fLastTime = now;
	return m_fLength;
}
