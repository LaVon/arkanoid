#ifndef SHAPES_H
#define SHAPES_H
#include <math.h>
#include <windows.h>
#include <gl\GL.h>
#include <gl\GLU.h>
#include <gl\glaux.h>

//��� ���� �������� - ������ �� ��������� �������� � �� �� ����� ��� ��������
#define BORDER_LEFT 0
#define BORDER_RIGHT 28.5
#define BORDER_TOP 20
#define BORDER_BOTTOM 0

class Shape
{
public:
	GLuint m_shapeTexture;
	bool LoadShapeTexture(char* _image);
};

class Cube : public Shape
{
public:
	//���������� ����
	GLfloat x, y, z;
	//������� ����
	GLfloat A[3], B[3], C[3], D[3], E[3], F[3], G[3], H[3];
	//������� ��������������� 
	GLfloat a, b, c;
};

class Sphere : public Shape
{
public:
	GLfloat x, y, z;
	GLfloat radius;

};
#endif