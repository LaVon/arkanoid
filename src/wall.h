#include "shapes.h"

class Brick : public Cube
{
public:
	Brick();
	~Brick();

	bool isAlive;
};

class Wall 
{
public:
	Wall();
	~Wall();
	Brick* m_brick;
	GLuint m_countDestroyed;
	GLuint wall_size;
	void DrawWall();
	void Restart();

private:
	//� ���������� ����� �������� ��������� ������ ����� (���������, ����, �������������)
	void BuildWall();
	
};