#include "ball.h"
#include <iostream>

using namespace std;

Ball::Ball()
{
	radius = 0.5;
	Restart();
}
 
Ball::~Ball()
{
}

void Ball::Restart()
{
	x = 14.0;
	y = 1.0;
	z = -7.0;
	m_fStartSpeed = 0.01;
	m_fSpeed = m_fStartSpeed;
	isUp = true;
	isRight = true;
	isLeft = false;
}

void Ball::DrawBall()
{
	GLUquadric *quardic = gluNewQuadric();
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, m_shapeTexture);
	glColor3f(1.0, 1.0, 1.0);
	gluQuadricDrawStyle(quardic, GLU_FILL);
	glTranslatef(x, y, z);
	gluQuadricTexture(quardic, 1);
	gluSphere(quardic, radius, 20, 20);
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
	gluDeleteQuadric(quardic);
}

void Ball::MoveBall(float _dt)
{
	if (isLeft)
		x -= m_fSpeed * _dt;
	else if (isRight)
		     x += m_fSpeed * _dt;
	if (isUp)
		y += m_fSpeed * _dt;
	else if (!isUp)
		y -= m_fSpeed * _dt;
}

void Ball::FallBall()
{
	y = -BALL_FALL;
}
void Ball::ChangeXDir()
{
	if (isRight)
	{
		isRight = false;
		isLeft = true;
	}
	else if (isLeft)
	{
		isLeft = false;
		isRight = true;
	}
	else
	isRight = true;
}

void Ball::ChangeYDir()
{
	if (!isLeft && !isRight)
		ChangeXDir();
	if (isUp)
		isUp = false;
	else 
		isUp = true;
}
