#ifndef _TIMER_H_
#define _TIMER_H_

#include <windows.h>

class GLTimer
{
public:
	GLTimer();
	float GetGLTime();

private:
	bool	m_bPerformance;		// whether the performance timer is available
	float	m_bResolution;			// timer resolution
	DWORD	m_dwStart;			// multimedia timer start
	__int64	m_iPerfStart;			// performance timer start
};

class GLFrame
{
public:
	GLFrame();
	// Update frame counter *call once a frame*
	float Update();
	// Get frames per second
	int GetFPS()		{ return m_iFps; };
	// get the time of the last frame
	float GetLength()	{ return m_fLength; };

private:
	GLTimer	*m_pTimer;		// a timer object
	float	m_fLastTime;	// last time called
	int		m_iFps;		// frames per second
	float	m_fLength;		// length of frame
};

#endif;